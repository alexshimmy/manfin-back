FROM adoptopenjdk:11-jre-hotspot
ARG JAR_FILE=./build/libs/*.jar
COPY ${JAR_FILE} /var/app/app.jar
