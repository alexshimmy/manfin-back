# README

## Swagger documentation

https://springdoc.org/

* Generate OpenAPI spec (build/openapi.json)
    * `./gradlew clean generateOpenApiDocs`


* Swagger HTML page:
    * `<SERVER_URL>/swagger-ui/index.html`

## Run tests

`./gradlew clean test`

## Run App + PostgreSQL using docker-compose

- set environment variables:
    - POSTGRES_USER - postgres username
    - POSTGRES_PASSWORD - postgres password
    - SERVER_PORT - Manfin application port
    - JWT_SECRET - secret phrase for JWT encryption
    - JWT_EXPIRATION - token expiration in sec
    - ADMIN_USERNAME - initial username (email format)
    - ADMIN_PASSWORD - initial password (min length = 7)


- run application (examples):
    - ```export ADMIN_USERNAME=adminuser@mailinator.com; export ADMIN_PASSWORD=AdminPassword1; sh run_app.sh ```
    - ```sh run_app.sh ```  
  