#!/usr/bin/env bash

# Environment variables:
#   POSTGRES_USER - postgres username
#   POSTGRES_PASSWORD - postgres password
#   SERVER_PORT - Manfin application port
#   JWT_SECRET - secret phrase for JWT encryption
#   JWT_EXPIRATION - token expiration in sec
#   ADMIN_USERNAME - initial username (email format)
#   ADMIN_PASSWORD - initial password (min length = 7)

./gradlew clean bootJar
docker-compose down
docker rmi manfin # remove image
docker-compose up -d
