package by.manfin.manfinback.authentication;

import by.manfin.manfinback.model.user.User;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@DisplayName("JwtUtility test suite")
class JwtUtilityTest {

    static private final Faker faker = new Faker();
    static private final User user = new User();

    @Autowired
    private JwtUtility jwtUtility;

    @BeforeAll
    static void beforeAll() {
        user.setUsername(faker.name().username());
        user.setPassword(faker.internet().password(7, 20));
        user.setEnabled(true);
    }

    @Test
    @DisplayName("Extract username")
    void extractUsername() {
        //given
        var tokenData = jwtUtility.generateTokens(user).orElseThrow();
        //when
        var extractedUsername = jwtUtility.extractUsername(tokenData.getToken());
        var extractedUsernameFromRefreshToken = jwtUtility.extractUsername(tokenData.getRefreshToken());
        //then
        assertEquals(user.getUsername(), extractedUsername.orElseThrow());
        assertEquals(user.getUsername(), extractedUsernameFromRefreshToken.orElseThrow());
    }

    @Test
    @DisplayName("Is token expired")
    void isTokenExpired() {
        //given
        var tokenData = jwtUtility.generateTokens(user).orElseThrow();
        //when
        var isTokenExpired = jwtUtility.isTokenExpired(tokenData.getToken());
        var isRefreshTokenExpired = jwtUtility.isTokenExpired(tokenData.getRefreshToken());
        //then
        assertFalse(isTokenExpired);
        assertFalse(isRefreshTokenExpired);
    }

    @Test
    @DisplayName("Validate token")
    void validateToken() {
        //given
        var tokenData = jwtUtility.generateTokens(user).orElseThrow();
        //when
        var isTokenValid = jwtUtility.validateToken(tokenData.getToken(), user);
        var isRefreshTokenValid = jwtUtility.validateToken(tokenData.getRefreshToken(), user);
        //then
        assertTrue(isTokenValid);
        assertTrue(isRefreshTokenValid);
    }

    @Test
    @DisplayName("Validate wrong token")
    void validateWrongToken() {
        //given
        var token = faker.internet().uuid();
        //when
        var isTokenValid = jwtUtility.validateToken(token, user);
        //then
        assertFalse(isTokenValid);
    }

    @Test
    @DisplayName("Generate token")
    void generateToken() {
        //given-when
        var token = jwtUtility.generateTokens(user);
        //then
        assertFalse(token.isEmpty());
    }

    @Test
    @DisplayName("Generate token with null user")
    void generateTokenNullUser() {
        //given-when
        var token = jwtUtility.generateTokens(null);
        //then
        assertTrue(token.isEmpty());
    }

    @Test
    @DisplayName("Is refresh token")
    void isRefreshToken() {
        //given-when
        var token = jwtUtility.generateTokens(user);
        //then
        assertTrue(jwtUtility.isRefreshToken(token.orElseThrow().getRefreshToken()));
    }
}