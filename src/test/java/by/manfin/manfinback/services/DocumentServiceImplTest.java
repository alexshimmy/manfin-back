package by.manfin.manfinback.services;

import by.manfin.manfinback.model.Document;
import by.manfin.manfinback.model.user.User;
import by.manfin.manfinback.model.user.UserRole;
import by.manfin.manfinback.repository.DocumentRepository;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@DisplayName("Document service test suite")
class DocumentServiceImplTest {

    @Autowired
    private DocumentService documentService;

    @MockBean
    private DocumentRepository documentRepository;

    static User user = new User();
    static User mockUser = Mockito.mock(User.class);

    static Document document = new Document();

    @BeforeEach
    void setUp() {
        mockUser = Mockito.mock(User.class);
    }

    @BeforeAll
    static void beforeAll() {
        var faker = new Faker();
        document.setId(1L);
        document.setDocumentNumber(faker.internet().uuid());

        user.setDocuments(List.of(document));
        user.setId(1L);
        user.setPassword(faker.internet().password(7, 30));
        user.setRole(UserRole.ROLE_ADMIN.name());
        user.setFirstname(faker.internet().emailAddress());
        user.setLastname(faker.internet().emailAddress());
        user.setUsername(faker.internet().emailAddress());
        user.setEnabled(true);
    }

    @Test
    @DisplayName("Get all documents")
    void getAllDocuments() {
        //given-when
        var documents = documentService.getAllDocuments(user);
        //then
        assertEquals(documents.size(), 1);
        assertEquals(documents.stream().findFirst().orElseThrow().getId(), document.getId());
    }

    @Test
    @DisplayName("Get document by id")
    void getDocument() {
        //given-when
        var dbDocument = documentService.getDocument(user, document.getId());
        //then
        assertEquals(document.getId(), dbDocument.getId());
    }

    @Test
    @DisplayName("Get document by nonexistent id")
    void getDocumentByNonexistentId() {
        //given-when
        var exception = assertThrows(
                ResponseStatusException.class,
                () -> documentService.getDocument(user, document.getId() + 1)
        );
        //then
        assertSame(exception.getStatus(), HttpStatus.NOT_FOUND);
    }

    @Test
    @DisplayName("Create new document")
    void createDocument() {
        //given
        var newDocument = new Document();
        newDocument.setId(2L);
        newDocument.setDocumentNumber("SecondNumber-2021");
        Mockito.doReturn(List.of(newDocument)).when(mockUser).getDocuments();
        Mockito.doReturn(document).when(documentRepository).save(ArgumentMatchers.any(Document.class));
        //when
        var dbDocument = documentService.createDocument(mockUser, document);
        //then
        Mockito.verify(documentRepository, Mockito.times(1)).save(ArgumentMatchers.any(Document.class));
        assertEquals(dbDocument.getId(), document.getId());
        Mockito.verify(mockUser, Mockito.times(1)).getDocuments();
    }

    @Test
    @DisplayName("Create new document with duplicated number")
    void createDocumentDuplicatedNumber() {
        //given
        var newDocument = new Document();
        newDocument.setDocumentNumber(document.getDocumentNumber());
        Mockito.doReturn(List.of(newDocument)).when(mockUser).getDocuments();
        //when
        var exception = assertThrows(
                ResponseStatusException.class,
                () -> documentService.createDocument(mockUser, document)
        );
        //then
        assertSame(exception.getStatus(), HttpStatus.BAD_REQUEST);
        Mockito.verify(mockUser, Mockito.times(1)).getDocuments();
    }

    @Test
    @DisplayName("Update document")
    void updateDocument() {
        //given
        var newDocument = new Document();
        newDocument.setDocumentNumber("DocumentNumber");
        Mockito.doReturn(List.of(document)).when(mockUser).getDocuments();
        Mockito.doReturn(newDocument).when(documentRepository).save(ArgumentMatchers.any(Document.class));
        //when
        var updatedDocument = documentService.updateDocument(mockUser, document.getId(), newDocument);
        //then
        assertEquals(updatedDocument.getDocumentNumber(), newDocument.getDocumentNumber());
        Mockito.verify(mockUser, Mockito.times(1)).getDocuments();
    }

    @Test
    @DisplayName("Update nonexistent document")
    void updateNonexistentDocument() {
        //given
        var newDocument = new Document();
        newDocument.setDocumentNumber("DocumentNumber");
        Mockito.doReturn(Collections.EMPTY_LIST).when(mockUser).getDocuments();
        //when
        var exception = assertThrows(
                ResponseStatusException.class,
                () -> documentService.updateDocument(mockUser, document.getId(), newDocument)
        );
        //then
        assertSame(exception.getStatus(), HttpStatus.NOT_FOUND);
        Mockito.verify(mockUser, Mockito.times(1)).getDocuments();
    }

    @Test
    @DisplayName("Delete document")
    void deleteDocument() {
        //given
        Mockito.doReturn(List.of(document)).when(mockUser).getDocuments();
        //then
        documentService.deleteDocument(mockUser, document.getId());
        //then
        Mockito.verify(mockUser, Mockito.times(1)).getDocuments();
        Mockito.verify(documentRepository, Mockito.times(1)).delete(ArgumentMatchers.any(Document.class));
    }

    @Test
    @DisplayName("Delete nonexistent document")
    void deleteNonexistentDocument() {
        //given
        Mockito.doReturn(Collections.EMPTY_LIST).when(mockUser).getDocuments();
        //when
        var exception = assertThrows(
                ResponseStatusException.class,
                () -> documentService.deleteDocument(mockUser, document.getId())
        );
        //then
        assertSame(exception.getStatus(), HttpStatus.NOT_FOUND);
        Mockito.verify(mockUser, Mockito.times(1)).getDocuments();
        Mockito.verify(documentRepository, Mockito.times(0)).delete(ArgumentMatchers.any(Document.class));
    }
}
