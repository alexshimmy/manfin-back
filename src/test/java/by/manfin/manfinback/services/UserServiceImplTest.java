package by.manfin.manfinback.services;

import by.manfin.manfinback.model.RefreshToken;
import by.manfin.manfinback.model.authentication.UsernamePasswordRequest;
import by.manfin.manfinback.model.user.User;
import by.manfin.manfinback.model.user.UserRole;
import by.manfin.manfinback.repository.RefreshTokenRepository;
import by.manfin.manfinback.repository.UserRepository;
import com.github.javafaker.Faker;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@DisplayName("User service test suite")
class UserServiceImplTest {

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;
    @MockBean
    private RefreshTokenRepository refreshTokenRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private static User fakeUser;
    private static String username;
    private static String anotherUsername;
    private static String firstname;
    private static String password;

    @BeforeAll
    static void setUp() {
        var faker = new Faker();

        username = faker.internet().emailAddress();
        anotherUsername = faker.internet().emailAddress();
        firstname = faker.name().firstName();
        password = faker.internet().password(7, 30);

        fakeUser = new User();
        fakeUser.setUsername(faker.internet().emailAddress());
        fakeUser.setEnabled(true);
        fakeUser.setFirstname(faker.internet().emailAddress());
        fakeUser.setLastname(faker.internet().emailAddress());
        fakeUser.setPassword(faker.internet().password(7, 30));
    }

    @Test
    @DisplayName("Register user")
    void registerUser() {
        //given
        var user = new User();
        user.setPassword(password);
        //when
        var isUserCreated = userService.register(user);
        //then
        assertTrue(isUserCreated);
        assertTrue(user.getEnabled());
        assertTrue(CoreMatchers.is(user.getRole()).matches(UserRole.ROLE_USER.name()));
        Mockito.verify(userRepository, Mockito.times(1)).save(user);
        assertTrue(passwordEncoder.matches(password, user.getPassword()));
    }

    @Test
    @DisplayName("Register existing user")
    void registerExistingUser() {
        //given
        Mockito.doReturn(Optional.of(new User())).when(userRepository).findByUsername(fakeUser.getUsername());
        //when
        var exception = assertThrows(
                ResponseStatusException.class,
                () -> userService.register(fakeUser)
        );
        //then
        assertSame(exception.getStatus(), HttpStatus.BAD_REQUEST);
    }

    @Test
    @DisplayName("Update not existing user")
    void updateNonexistentUser() {
        //given-when
        var exception = assertThrows(
                ResponseStatusException.class,
                () -> userService.update(username, fakeUser)
        );
        //then
        assertSame(exception.getStatus(), HttpStatus.BAD_REQUEST);
    }

    @Test
    @DisplayName("Update existing user")
    void updateExistingUser() {
        //given
        Mockito.doReturn(Optional.of(new User())).when(userRepository).findByUsername(anotherUsername);
        Mockito.doReturn(fakeUser).when(userRepository).save(ArgumentMatchers.any(User.class));
        //when
        var updatedUser = userService.update(anotherUsername, fakeUser);
        //then
        assertSame(fakeUser.getUsername(), updatedUser.getUsername());
        assertSame(fakeUser.getFirstname(), updatedUser.getFirstname());
        assertSame(fakeUser.getLastname(), updatedUser.getLastname());
        assertSame(fakeUser.getPassword(), updatedUser.getPassword());
    }

    @Test
    @DisplayName("Get auth tokens credentials")
    void getAuthToken() {
        //given
        fakeUser.setPassword(passwordEncoder.encode(password));
        Mockito.doReturn(Optional.of(fakeUser)).when(userRepository).findByUsername(fakeUser.getUsername());
        var request = new UsernamePasswordRequest(fakeUser.getUsername(), password);
        //when
        var token = userService.authentication(request);
        //then
        assertFalse(token.getToken().isBlank());
        assertFalse(token.getRefreshToken().isBlank());
    }

    @Test
    @DisplayName("Get auth token by wrong user credentials")
    void getAuthTokenWrongCredentials() {
        //given
        Mockito.doReturn(Optional.of(fakeUser)).when(userRepository).findByUsername(username);
        var request = new UsernamePasswordRequest(firstname, password);
        //when
        var token = userService.authentication(request);
        //then
        assertNull(token);
    }

    @Test
    @DisplayName("Load user by username")
    void loadUserByUsername() {
        //given
        Mockito.doReturn(Optional.of(fakeUser)).when(userRepository).findByUsername(fakeUser.getUsername());
        //when
        var user = userService.loadUserByUsername(fakeUser.getUsername());
        //then
        assertNotNull(user);
    }

    @Test
    @DisplayName("Load user by wrong username")
    void loadUserByNonexistentUsername() {
        //given
        Mockito.doReturn(Optional.empty()).when(userRepository).findByUsername(fakeUser.getUsername());
        //when
        var user = userService.loadUserByUsername(fakeUser.getUsername());
        //then
        assertNull(user);
    }

    @Test
    @DisplayName("Refresh token")
    void refreshToken() {
        //given
        var user = new User();
        user.setUsername(fakeUser.getUsername());
        user.setEnabled(true);
        user.setPassword(passwordEncoder.encode(password));

        Mockito.doReturn(Optional.of(user)).when(userRepository).findByUsername(user.getUsername());
        var authRequest = new UsernamePasswordRequest(
                user.getUsername(),
                password
        );
        var token = userService.authentication(authRequest);

        var refreshToken = new RefreshToken();
        refreshToken.setUser(user);
        refreshToken.setToken(token.getRefreshToken());
        Mockito.doReturn(Optional.of(refreshToken))
                .when(refreshTokenRepository)
                .findByToken(ArgumentMatchers.any(String.class));
        //when
        var refreshedToken = userService.refreshToken(token.getRefreshToken());
        //then
        assertFalse(refreshedToken.getRefreshToken().isEmpty());
        assertFalse(refreshedToken.getToken().isEmpty());
    }
}
