package by.manfin.manfinback.services;

import by.manfin.manfinback.model.Currency;
import by.manfin.manfinback.repository.CurrencyRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@DisplayName("Currency service test suite")
class CurrencyServiceImplTest {

    static Currency currency = new Currency(1L, "American Dollar", "USD", null);

    @Autowired
    private CurrencyService currencyService;

    @MockBean
    private CurrencyRepository currencyRepository;

    @Test
    @DisplayName("Get all currencies")
    void getAllCurrencies() {
        //given
        Mockito.doReturn(List.of(currency)).when(currencyRepository).findAll();
        //when
        var allCurrencies = currencyService.getAllCurrencies();
        //then
        assertTrue(allCurrencies.contains(currency));
        assertEquals(1, allCurrencies.size());
        Mockito.verify(currencyRepository, Mockito.times(1)).findAll();
    }

    @Test
    @DisplayName("Get existing currency by id")
    void getCurrency() {
        //given
        Mockito.doReturn(Optional.of(currency)).when(currencyRepository).findById(currency.getId());
        //when
        var serviceCurrency = currencyService.getCurrency(currency.getId());
        //then
        assertEquals(currency.getId(), serviceCurrency.getId());
        Mockito.verify(currencyRepository, Mockito.times(1)).findById(currency.getId());
    }

    @Test
    @DisplayName("Get nonexistent currency by id")
    void getNonexistentCurrency() {
        //given
        Mockito.doReturn(Optional.empty()).when(currencyRepository).findById(currency.getId());
        //when
        var exception = assertThrows(
                ResponseStatusException.class,
                () -> currencyService.getCurrency(currency.getId())
        );
        //then
        assertEquals(exception.getStatus(), HttpStatus.NOT_FOUND);
        Mockito.verify(currencyRepository, Mockito.times(1)).findById(currency.getId());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @DisplayName("Create new currency")
    void createCurrency() {
        //given
        Mockito.doReturn(Optional.empty()).when(currencyRepository).findByShortName(currency.getShortName());
        Mockito.doReturn(currency).when(currencyRepository).save(currency);
        //when
        var serviceCurrency = currencyService.createCurrency(currency);
        //then
        assertEquals(serviceCurrency.getId(), currency.getId());
        Mockito.verify(currencyRepository, Mockito.times(1)).findByShortName(currency.getShortName());
        Mockito.verify(currencyRepository, Mockito.times(1)).save(currency);
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @DisplayName("Create existing currency")
    void createExistingCurrency() {
        //given
        Mockito.doReturn(Optional.of(currency)).when(currencyRepository).findByShortName(currency.getShortName());
        //when
        var exception = assertThrows(
                ResponseStatusException.class,
                () -> currencyService.createCurrency(currency)
        );
        //then
        assertSame(exception.getStatus(), HttpStatus.BAD_REQUEST);
        Mockito.verify(currencyRepository, Mockito.times(1)).findByShortName(currency.getShortName());
        Mockito.verify(currencyRepository, Mockito.times(0)).save(currency);
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @DisplayName("Update existing currency")
    void updateCurrency() {
        //given
        var currencyData = new Currency(null, "Russian Ruble", "RUB", null);
        Mockito.doReturn(Optional.of(currency)).when(currencyRepository).findById(currency.getId());
        Mockito.doReturn(new Currency(currency.getId(), currencyData.getName(), currencyData.getShortName(), null))
                .when(currencyRepository).save(ArgumentMatchers.any(Currency.class));
        //when
        var returnedCurrency = currencyService.updateCurrency(currency.getId(), currencyData);
        //then
        Mockito.verify(currencyRepository, Mockito.times(1)).findById(currency.getId());
        Mockito.verify(currencyRepository, Mockito.times(1)).save(ArgumentMatchers.any(Currency.class));
        assertSame(returnedCurrency.getName(), currency.getName());
        assertSame(returnedCurrency.getShortName(), currency.getShortName());
        assertSame(returnedCurrency.getId(), currency.getId());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @DisplayName("Update nonexistent currency")
    void updateNonexistentCurrency() {
        //given
        Mockito.doReturn(Optional.empty()).when(currencyRepository).findById(currency.getId());
        //when
        var exception = assertThrows(
                ResponseStatusException.class,
                () -> currencyService.updateCurrency(currency.getId(), currency)
        );
        //then
        Mockito.verify(currencyRepository, Mockito.times(1)).findById(currency.getId());
        Mockito.verify(currencyRepository, Mockito.times(0)).save(ArgumentMatchers.any(Currency.class));
        assertSame(exception.getStatus(), HttpStatus.BAD_REQUEST);
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @DisplayName("Delete existing currency")
    void deleteCurrency() {
        //given
        Mockito.doReturn(Optional.of(currency)).when(currencyRepository).findById(currency.getId());
        //when
        currencyService.deleteCurrency(currency.getId());
        //then
        Mockito.verify(currencyRepository, Mockito.times(1)).delete(currency);
        Mockito.verify(currencyRepository, Mockito.times(1)).findById(currency.getId());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @DisplayName("Delete nonexistent currency")
    void deleteNonexistentCurrency() {
        //given
        Mockito.doReturn(Optional.empty()).when(currencyRepository).findById(currency.getId());
        //when-then
        var exception = assertThrows(
                ResponseStatusException.class,
                () -> currencyService.deleteCurrency(currency.getId())
        );
        assertSame(exception.getStatus(), HttpStatus.BAD_REQUEST);
        Mockito.verify(currencyRepository, Mockito.times(0)).delete(ArgumentMatchers.any(Currency.class));
        Mockito.verify(currencyRepository, Mockito.times(1)).findById(currency.getId());
    }
}