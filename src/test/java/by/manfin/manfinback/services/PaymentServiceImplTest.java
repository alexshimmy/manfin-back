package by.manfin.manfinback.services;

import by.manfin.manfinback.model.Document;
import by.manfin.manfinback.model.Payment;
import by.manfin.manfinback.model.user.User;
import by.manfin.manfinback.repository.PaymentRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@DisplayName("Payment service test suite")
class PaymentServiceImplTest {

    @Autowired
    private PaymentService paymentService;

    @MockBean
    private DocumentService documentService;

    @MockBean
    private PaymentRepository paymentRepository;

    static Payment payment = new Payment();
    static User user = new User();
    static Document document = new Document();

    @BeforeAll
    static void beforeAll() {
        document.setId(1L);
        document.setDocumentNumber("DocNumber-2022");

        payment.setId(1L);
        payment.setDocumentId(document.getId());

        document.setPayments(List.of(payment));

        user.setDocuments(List.of(document));
    }

    @Test
    @DisplayName("Get all payments")
    void getAllPayments() {
        //given
        Mockito.doReturn(document).when(documentService).getDocument(user, document.getId());
        //when
        var payments = paymentService.getAllPayments(user, document.getId());
        //then
        assertEquals(payments.size(), document.getPayments().size());
        assertEquals(payments.stream().findFirst().orElseThrow().getDocumentId(), document.getId());
    }

    @Test
    @DisplayName("Get payment by id")
    void getPayment() {
        //given
        Mockito.doReturn(document).when(documentService).getDocument(user, document.getId());
        //when
        var docPayment = paymentService.getPayment(user, document.getId(), payment.getId());
        //then
        assertEquals(docPayment.getId(), payment.getId());
    }

    @Test
    @DisplayName("Get payment by nonexistent id")
    void getNonexistentPayment() {
        //given
        Mockito.doReturn(document).when(documentService).getDocument(user, document.getId());
        //when-then
        var exception = assertThrows(
                ResponseStatusException.class,
                () -> paymentService.getPayment(user, document.getId(), payment.getId() + 1)
        );
        assertEquals(exception.getStatus(), HttpStatus.NOT_FOUND);
        Mockito.verify(documentService, Mockito.times(1)).getDocument(user, document.getId());
    }

    @Test
    @DisplayName("Create payment")
    void createPayment() {
        //given
        var newDocument = new Document();
        newDocument.setId(2L);
        Mockito.doReturn(document).when(documentService).getDocument(user, document.getId());
        Mockito.doReturn(payment).when(paymentRepository).save(payment);
        //when
        var savedPayment = paymentService.createPayment(user, newDocument.getId(), payment);
        //then
        assertEquals(newDocument.getId(), savedPayment.getDocumentId());
    }

    @Test
    @DisplayName("Create null payment")
    void createNullPayment() {
        //given
        var newDocument = new Document();
        newDocument.setId(2L);
        newDocument.setDocumentNumber("Num123");
        Mockito.doReturn(document).when(documentService).getDocument(user, document.getId());
        //when-then
        var exception = assertThrows(
                ResponseStatusException.class,
                () -> paymentService.createPayment(user, newDocument.getId(), null)
        );

        assertEquals(exception.getStatus(), HttpStatus.BAD_REQUEST);
    }

    @Test
    @DisplayName("Update payment")
    void updatePayment() {
        //given
        var newPaymentData = new Payment();
        newPaymentData.setId(3L);
        Mockito.doReturn(document).when(documentService).getDocument(user, document.getId());
        Mockito.doReturn(newPaymentData).when(paymentRepository).save(ArgumentMatchers.any(Payment.class));
        //when
        var updatedPayment = paymentService.updatePayment(user, document.getId(), payment.getId(), newPaymentData);
        //then
        assertEquals(updatedPayment.getId(), newPaymentData.getId());
    }

    @Test
    @DisplayName("Update payment by null data")
    void updatePaymentByNullPaymentData() {
        //given
        Mockito.doReturn(document).when(documentService).getDocument(user, document.getId());
        //when-then
        var exception = assertThrows(
                ResponseStatusException.class,
                () -> paymentService.updatePayment(user, document.getId(), payment.getId(), null)
        );

        assertEquals(exception.getStatus(), HttpStatus.BAD_REQUEST);
    }

    @Test
    @DisplayName("Delete payment")
    void deletePayment() {
        //given
        Mockito.doReturn(document).when(documentService).getDocument(user, document.getId());
        //when
        paymentService.deletePayment(user, document.getId(), payment.getId());
        //then
        Mockito.verify(documentService, Mockito.times(1)).getDocument(user, document.getId());
        Mockito.verify(paymentRepository, Mockito.times(1)).delete(ArgumentMatchers.any(Payment.class));
    }
}