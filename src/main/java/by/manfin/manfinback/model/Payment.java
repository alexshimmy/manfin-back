package by.manfin.manfinback.model;

import by.manfin.manfinback.validation.Marker;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "payments", schema = "manfin")
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    @Column(name = "document_id")
    private Long documentId;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "document_id", insertable = false, updatable = false)
    private Document document;

    @NotNull(groups = Marker.OnCreate.class)
    @Column(name = "date")
    private LocalDate date;

    @NotNull(groups = Marker.OnCreate.class)
    @Column(name = "sum", nullable = false, precision = 50, scale = 2)
    private Double sum;

    public void updateData(Payment paymentData) {
        if (paymentData.getDate() != null) {
            this.setDate(paymentData.getDate());
        }
        if (paymentData.getSum() != null) {
            this.setSum(paymentData.getSum());
        }
    }
}