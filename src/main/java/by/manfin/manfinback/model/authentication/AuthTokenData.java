package by.manfin.manfinback.model.authentication;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class AuthTokenData {
    private String token;
    private String refreshToken;
    private Date expiry;
}
