package by.manfin.manfinback.model.authentication;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
public class UsernamePasswordRequest {
    @Email
    @Length(max = 100)
    private String username;

    @NotBlank
    @Length(min = 7, max = 100)
    private String password;

    public UsernamePasswordRequest(String username, String password) {
        setUsername(username);
        setPassword(password);
    }
}
