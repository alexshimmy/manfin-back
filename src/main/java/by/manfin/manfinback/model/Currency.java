package by.manfin.manfinback.model;

import by.manfin.manfinback.validation.Marker;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.LinkedHashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "currency", schema = "manfin")
@AllArgsConstructor
@NoArgsConstructor
public class Currency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;

    @NotBlank(groups = Marker.OnCreate.class)
    @Column(name = "name", nullable = false)
    private String name;

    @NotBlank(groups = Marker.OnCreate.class)
    @Column(name = "short_name", nullable = false)
    private String shortName;

    @JsonIgnore
    @OneToMany(mappedBy = "currency")
    private Set<Document> documents = new LinkedHashSet<>();
}