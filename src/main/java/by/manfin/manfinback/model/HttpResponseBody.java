package by.manfin.manfinback.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;


@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HttpResponseBody<T> {
    String message;
    T data;
}
