package by.manfin.manfinback.model;

import by.manfin.manfinback.model.user.User;
import by.manfin.manfinback.validation.Marker;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "documents", schema = "manfin")
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "document_number")
    @Length(max = 100)
    @NotBlank(groups = Marker.OnCreate.class)
    private String documentNumber;

    @Column(name = "user_id")
    @JsonIgnore
    private Long userId;

    @NotBlank(groups = Marker.OnCreate.class)
    @Length(max = 100)
    private String name;

    @Length(max = 500)
    private String description;

    @NotNull(groups = Marker.OnCreate.class)
    private Double sum;

    @NotNull(groups = Marker.OnCreate.class)
    @Column(name = "start_date")
    private LocalDate startDate;

    @NotNull(groups = Marker.OnCreate.class)
    @Column(name = "end_date")
    private LocalDate endDate;

    @NotNull(groups = Marker.OnCreate.class)
    @Column(name = "paid_out")
    private Double paidOut;

    @NotNull(groups = Marker.OnCreate.class)
    @Column(name = "currency_id")
    private Long currencyId;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "currency_id", insertable = false, updatable = false)
    private Currency currency;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User user;

    @OneToMany(mappedBy = "document", fetch = FetchType.EAGER)
    private List<Payment> payments = new LinkedList<>();

    public void updateData(Document documentData) {
        if (documentData.getDocumentNumber() != null) {
            this.setDocumentNumber(documentData.getDocumentNumber());
        }
        if (documentData.getStartDate() != null) {
            this.setStartDate(documentData.getStartDate());
        }
        if (documentData.getEndDate() != null) {
            this.setEndDate(documentData.getEndDate());
        }
        if (documentData.getName() != null) {
            this.setName(documentData.getName());
        }
        if (documentData.getDescription() != null) {
            this.setDescription(documentData.getDescription());
        }
        if (documentData.getPaidOut() != null) {
            this.setPaidOut(documentData.getPaidOut());
        }
        if (documentData.getSum() != null) {
            this.setSum(documentData.getSum());
        }
        if (documentData.getCurrencyId() != null) {
            this.setCurrencyId(documentData.getCurrencyId());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Document document = (Document) o;
        return id != null && Objects.equals(id, document.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
