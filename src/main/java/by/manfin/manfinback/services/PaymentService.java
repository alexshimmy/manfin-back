package by.manfin.manfinback.services;

import by.manfin.manfinback.model.Payment;
import by.manfin.manfinback.model.user.User;

import java.util.Collection;

public interface PaymentService {

    Collection<Payment> getAllPayments(User user, Long documentId);

    Payment getPayment(User user, Long documentId, Long paymentId);

    Payment createPayment(User user, Long documentId, Payment payment);

    Payment updatePayment(User user, Long documentId, Long paymentId, Payment paymentData);

    void deletePayment(User user, Long documentId, Long paymentId);
}
