package by.manfin.manfinback.services;

import by.manfin.manfinback.model.Document;
import by.manfin.manfinback.model.user.User;

import java.util.Collection;

public interface DocumentService {

    Collection<Document> getAllDocuments(User user);

    Document getDocument(User user, Long documentId);

    Document createDocument(User user, Document document);

    Document updateDocument(User user, Long documentId, Document documentData);

    void deleteDocument(User user, Long documentId);
}
