package by.manfin.manfinback.services;

import by.manfin.manfinback.authentication.JwtUtility;
import by.manfin.manfinback.model.RefreshToken;
import by.manfin.manfinback.model.authentication.AuthTokenData;
import by.manfin.manfinback.model.authentication.UsernamePasswordRequest;
import by.manfin.manfinback.model.user.User;
import by.manfin.manfinback.model.user.UserRole;
import by.manfin.manfinback.repository.RefreshTokenRepository;
import by.manfin.manfinback.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;
    private final RefreshTokenRepository refreshTokenRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtUtility jwtUtility;

    public UserServiceImpl(
            UserRepository repository,
            RefreshTokenRepository refreshTokenRepository,
            PasswordEncoder passwordEncoder,
            JwtUtility jwtUtility
    ) {
        this.repository = repository;
        this.refreshTokenRepository = refreshTokenRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtUtility = jwtUtility;
    }

    @Override
    public boolean register(User user) {
        User dbUser = loadUserByUsername(user.getUsername());
        if (dbUser != null) {
            var errorMessage = String.format("Cannot register new user: '%s' already exists", dbUser.getUsername());
            log.warn(errorMessage);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorMessage);
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setEnabled(true);
        user.setRole(UserRole.ROLE_USER.name());
        repository.save(user);
        log.info(String.format("User '%s' has been registered", user.getUsername()));

        return true;
    }

    @Override
    public User update(String username, User userData) {
        User dbUser = loadUserByUsername(username);
        if (dbUser == null) {
            var errorMessage = String.format("Cannot find user: '%s'", username);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorMessage);
        }
        if (userData.getPassword() != null) {
            userData.setPassword(passwordEncoder.encode(userData.getPassword()));
        }
        if (userData.getUsername() != null && loadUserByUsername(userData.getUsername()) != null) {
            var errorMessage = String.format("User with username: '%s' already exists", userData.getUsername());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorMessage);
        }

        dbUser.updateData(userData);

        try {
            var updatedUser = repository.save(dbUser);
            log.info(String.format("User data for user '%s' was updated", dbUser.getUsername()));

            return updatedUser;
        } catch (IllegalArgumentException exception) {
            var errorMessage = String.format("Cannot update user data '%s'", dbUser.getUsername());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorMessage, exception);
        }
    }

    @Override
    public AuthTokenData authentication(UsernamePasswordRequest usernamePasswordRequest) {
        User dbUser = loadUserByUsername(usernamePasswordRequest.getUsername());
        if (dbUser != null && dbUser.isEnabled()) {
            if (passwordEncoder.matches(usernamePasswordRequest.getPassword(), dbUser.getPassword())) {
                var tokensData = jwtUtility.generateTokens(dbUser)
                    .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.BAD_REQUEST, "Unable to generate tokens"
                    )
                );
                try {
                    var refreshToken = new RefreshToken();
                    refreshToken.setUserId(dbUser.getId());
                    refreshToken.setToken(tokensData.getRefreshToken());
                    refreshTokenRepository.deleteByUserId(dbUser.getId());
                    refreshTokenRepository.save(refreshToken);

                    return tokensData;
                } catch (Exception ex) {
                    log.error(String.format("Authorization error: userId=%s. %s", dbUser.getId(), ex.getMessage()));
                }
            }
        }

        log.warn(String.format("Cannot authorize user '%s'", usernamePasswordRequest.getUsername()));

        return null;
    }

    @Override
    public AuthTokenData refreshToken(String refreshToken) {
        var invalidTokenMessage = "Your refresh token is invalid";
        var tokenData = refreshTokenRepository.findByToken(refreshToken)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, invalidTokenMessage));
        var isTokenValid = jwtUtility.validateToken(tokenData.getToken(), tokenData.getUser());
        if (!isTokenValid) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, invalidTokenMessage);
        }

        return jwtUtility.generateTokens(tokenData.getUser())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unable to refresh tokens"));
    }

    @Override
    public User loadUserByUsername(String username) {
        var user = repository.findByUsername(username);

        return user.orElse(null);
    }
}
