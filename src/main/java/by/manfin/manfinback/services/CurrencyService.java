package by.manfin.manfinback.services;

import by.manfin.manfinback.model.Currency;

import java.util.Collection;

public interface CurrencyService {
    Collection<Currency> getAllCurrencies();
    Currency getCurrency(Long currencyId);
    Currency createCurrency(Currency currency);
    Currency updateCurrency(Long currencyId, Currency currencyData);
    void deleteCurrency(Long currencyId);
}
