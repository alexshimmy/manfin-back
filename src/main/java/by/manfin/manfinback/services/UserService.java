package by.manfin.manfinback.services;

import by.manfin.manfinback.model.authentication.AuthTokenData;
import by.manfin.manfinback.model.authentication.UsernamePasswordRequest;
import by.manfin.manfinback.model.user.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    boolean register(User user);

    User update(String username, User userData);

    AuthTokenData authentication(UsernamePasswordRequest usernamePasswordRequest);

    AuthTokenData refreshToken(String refreshToken);

}
