package by.manfin.manfinback.services;

import by.manfin.manfinback.authentication.IsAdmin;
import by.manfin.manfinback.model.Currency;
import by.manfin.manfinback.repository.CurrencyRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;

@Service
public class CurrencyServiceImpl implements CurrencyService {

    private final CurrencyRepository repository;

    public CurrencyServiceImpl(CurrencyRepository repository) {
        this.repository = repository;
    }

    @Override
    public Collection<Currency> getAllCurrencies() {

        return repository.findAll();
    }

    @Override
    public Currency getCurrency(Long currencyId) {
        var currency = repository.findById(currencyId);

        return currency.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Currency not found"));
    }

    @IsAdmin
    @Override
    public Currency createCurrency(Currency currency) {
        if (repository.findByShortName(currency.getShortName()).isPresent()) {
            var errorMessage = String.format("Currency with short name '%s' already exists", currency.getShortName());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorMessage);
        }

        try {
            return repository.save(currency);
        } catch (IllegalArgumentException exception) {
            var errorMessage = String.format("Cannot save currency with short name '%s'", currency.getShortName());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorMessage, exception);
        }
    }

    @IsAdmin
    @Override
    public Currency updateCurrency(Long currencyId, Currency currencyData) {
        var dbCurrency = repository.findById(currencyId).orElseThrow(() -> new ResponseStatusException(
                HttpStatus.BAD_REQUEST, String.format("Cannot find currency with id '%s'", currencyId)));

        try {
            if (currencyData.getName() != null) {
                dbCurrency.setName(currencyData.getName());
            }
            if (currencyData.getShortName() != null) {
                dbCurrency.setShortName(currencyData.getShortName());
            }

            return repository.save(dbCurrency);
        } catch (IllegalArgumentException exception) {
            var errorMessage = String.format("Cannot update currency with id '%s'", currencyId);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorMessage, exception);
        }
    }

    @IsAdmin
    @Override
    public void deleteCurrency(Long currencyId) {
        var errorMessage = String.format("Cannot find currency with id '%s'", currencyId);
        var currency = repository.findById(currencyId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, errorMessage));
        repository.delete(currency);
    }
}
