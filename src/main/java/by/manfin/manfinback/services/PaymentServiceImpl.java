package by.manfin.manfinback.services;

import by.manfin.manfinback.model.Payment;
import by.manfin.manfinback.model.user.User;
import by.manfin.manfinback.repository.PaymentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;

@Slf4j
@Service
public class PaymentServiceImpl implements PaymentService {

    private final DocumentService documentService;
    private final PaymentRepository paymentRepository;

    public PaymentServiceImpl(DocumentService documentService, PaymentRepository paymentRepository) {
        this.documentService = documentService;
        this.paymentRepository = paymentRepository;
    }

    @Override
    public Collection<Payment> getAllPayments(User user, Long documentId) {
        log.info(String.format("Get all payments: userId = %s", user.getId()));
        var document = documentService.getDocument(user, documentId);

        return document.getPayments();
    }

    @Override
    public Payment getPayment(User user, Long documentId, Long paymentId) {
        log.info(String.format("Get payment: userId = %s; paymentId = %s", user.getId(), paymentId));
        var document = documentService.getDocument(user, documentId);

        return document.getPayments()
                .stream().filter(pay -> pay.getId().equals(paymentId)).findFirst()
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Payment not found"));
    }

    @Override
    public Payment createPayment(User user, Long documentId, Payment payment) {
        log.info(String.format("Create payment: userId = %s", user.getId()));
        documentService.getDocument(user, documentId); //check if document exists
        try {
            payment.setDocumentId(documentId);
            return paymentRepository.save(payment);
        } catch (IllegalArgumentException | NullPointerException exception) {
            var errorMessage = "Cannot create payment";
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorMessage, exception);
        }
    }

    @Override
    public Payment updatePayment(User user, Long documentId, Long paymentId, Payment paymentData) {
        log.info(String.format("Update payment: userId = %s; paymentId = %s", user.getId(), paymentId));
        var payment = getPayment(user, documentId, paymentId); // check if payment exists
        try {
            payment.updateData(paymentData);
            return paymentRepository.save(payment);
        } catch (IllegalArgumentException | NullPointerException exception) {
            var errorMessage = "Cannot update payment";
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorMessage, exception);
        }
    }

    @Override
    public void deletePayment(User user, Long documentId, Long paymentId) {
        log.info(String.format("Delete payment: userId = %s; paymentId = %s", user.getId(), paymentId));
        var payment = getPayment(user, documentId, paymentId);
        try {
            paymentRepository.delete(payment);
        } catch (IllegalArgumentException exception) {
            var errorMessage = "Cannot remove payment";
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorMessage, exception);
        }
    }
}
