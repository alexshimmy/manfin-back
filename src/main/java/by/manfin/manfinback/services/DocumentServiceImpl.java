package by.manfin.manfinback.services;

import by.manfin.manfinback.model.Document;
import by.manfin.manfinback.model.user.User;
import by.manfin.manfinback.repository.DocumentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.Optional;

@Slf4j
@Service
public class DocumentServiceImpl implements DocumentService {

    private final DocumentRepository documentRepository;

    public DocumentServiceImpl(DocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }

    @Override
    public Collection<Document> getAllDocuments(User user) {
        log.info(String.format("getAllDocuments. userId = %s", user.getId()));
        return user.getDocuments();
    }

    @Override
    public Document getDocument(User user, Long documentId) {
        log.info(String.format("getDocument. userId = %s; documentId = %s", user.getId(), documentId));
        var document = user.getDocuments().stream()
                .filter(doc -> doc.getId().equals(documentId)).findFirst();

        return document.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Document not found"));
    }

    @Override
    public Document createDocument(User user, Document document) {
        log.info(String.format("createDocument. userId = %s", user.getId()));
        if (getDocumentByNumber(user, document.getDocumentNumber()).isPresent()) {
            var errorMessage = String.format("Document with number '%s' already exists", document.getDocumentNumber());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorMessage);
        }
        document.setUserId(user.getId());

        try {
            return documentRepository.save(document);
        } catch (IllegalArgumentException exception) {
            var errorMessage = String.format("Cannot save document with number '%s'", document.getDocumentNumber());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorMessage, exception);
        }
    }

    @Override
    public Document updateDocument(User user, Long documentId, Document documentData) {
        log.info(String.format("updateDocument. userId = %s; documentId = %s", user.getId(), documentId));
        var dbDocument = getDocument(user, documentId);
        dbDocument.updateData(documentData);

        try {
            return documentRepository.save(dbDocument);
        } catch (IllegalArgumentException exception) {
            var errorMessage = String.format("Cannot update document with id '%s'", documentId);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorMessage, exception);
        }
    }

    @Override
    public void deleteDocument(User user, Long documentId) {
        log.info(String.format("deleteDocument. userId = %s; documentId = %s", user.getId(), documentId));
        var document = getDocument(user, documentId);
        documentRepository.delete(document);
    }

    private Optional<Document> getDocumentByNumber(User user, String documentNumber) {
        log.info(String.format("getDocumentByNumber. userId = %s; documentNumber = %s", user.getId(), documentNumber));
        return user.getDocuments().stream()
                .filter(document -> document.getDocumentNumber().equals(documentNumber)).findFirst();
    }
}
