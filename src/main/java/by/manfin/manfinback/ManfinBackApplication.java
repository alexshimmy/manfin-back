package by.manfin.manfinback;

import by.manfin.manfinback.model.user.User;
import by.manfin.manfinback.model.user.UserRole;
import by.manfin.manfinback.repository.UserRepository;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityScheme;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.utility.RandomString;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Slf4j
@SpringBootApplication
public class ManfinBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(ManfinBackApplication.class, args);
    }

    @Bean
    public OpenAPI springShopOpenAPI() {
        return new OpenAPI()
            .components(
                new Components()
                    .addSecuritySchemes(
                        "bearer",
                        new SecurityScheme()
                            .type(SecurityScheme.Type.HTTP)
                            .scheme("bearer")
                            .bearerFormat("JWT")
                            .in(SecurityScheme.In.HEADER)
                    )
            ).info(new Info()
                .title("Manfin API")
                .version("v0.0.1"));
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Component
    public class CommandLineAppStartupRunner implements CommandLineRunner {

        private final UserRepository userRepository;

        public CommandLineAppStartupRunner(UserRepository userRepository) {
            this.userRepository = userRepository;
        }

        @Override
        public void run(String... args) {
            initAdminUser();
        }

        private void initAdminUser() {
            try {
                var existedAdmin = userRepository.findByRole(UserRole.ROLE_ADMIN.name());
                if (existedAdmin.isEmpty()) {
                    var admin = new User();

                    var admin_password =  System.getenv("ADMIN_PASSWORD");
                    if (admin_password == null) {
                        admin_password = RandomString.make(10);
                        log.info(String.format("Admin password: %s", admin_password));
                    }
                    var encodedPassword = passwordEncoder().encode(admin_password);
                    admin.setPassword(encodedPassword);

                    var admin_username =  System.getenv("ADMIN_USERNAME");
                    if (admin_username == null) {
                        admin_username = "admin@admin.mail";
                        log.info(String.format("Admin username: %s", admin_username));
                    }
                    admin.setUsername(admin_username);

                    admin.setFirstname("admin");
                    admin.setLastname("admin");
                    admin.setEnabled(true);
                    admin.setRole(UserRole.ROLE_ADMIN.name());

                    userRepository.save(admin);
                    log.info("Admin has been initialized");
                }
            } catch (Exception e) {
                log.error(String.format("Unable to initialize admin user: %s", e.getMessage()));
            }
        }
    }
}
