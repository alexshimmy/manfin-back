package by.manfin.manfinback.validation;

public interface Marker {

    interface OnCreate {
    }

    interface OnUpdate {
    }
}
