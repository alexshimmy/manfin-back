package by.manfin.manfinback.controllers;

import by.manfin.manfinback.model.Currency;
import by.manfin.manfinback.model.HttpResponseBody;
import by.manfin.manfinback.services.CurrencyService;
import by.manfin.manfinback.validation.Marker;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@RequestMapping("/currencies")
@RestController
@Validated
@SecurityRequirement(name = "bearer")
public class CurrencyController {

    private final CurrencyService currencyService;

    public CurrencyController(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @GetMapping
    public ResponseEntity<HttpResponseBody<Collection<Currency>>> getAllCurrencies() {
        var currencies = currencyService.getAllCurrencies();

        return ResponseEntity.ok(HttpResponseBody.<Collection<Currency>>builder()
            .data(currencies)
            .build());
    }

    @GetMapping("/{currencyId}")
    public ResponseEntity<HttpResponseBody<Currency>> getCurrency(@NotNull @PathVariable @NumberFormat Long currencyId) {
        Currency currency = currencyService.getCurrency(currencyId);

        return ResponseEntity.ok(HttpResponseBody.<Currency>builder()
            .data(currency)
            .build());
    }

    @PostMapping
    @Validated(Marker.OnCreate.class)
    public ResponseEntity<HttpResponseBody<Currency>> create(@RequestBody @Valid Currency currency) {
        var result = currencyService.createCurrency(currency);

        return ResponseEntity.ok(HttpResponseBody.<Currency>builder()
            .message("Currency saved successfully")
            .data(result)
            .build());
    }

    @PutMapping("/{currencyId}")
    public ResponseEntity<HttpResponseBody<Currency>> update(
        @RequestBody @Valid Currency currency,
        @NotNull @PathVariable @NumberFormat Long currencyId
    ) {
        var result = currencyService.updateCurrency(currencyId, currency);

        return ResponseEntity.ok(HttpResponseBody.<Currency>builder()
            .message("Currency updated successfully")
            .data(result)
            .build());
    }

    @DeleteMapping("/{currencyId}")
    public ResponseEntity<HttpResponseBody<Void>> delete(@NotNull @PathVariable @NumberFormat Long currencyId) {
        currencyService.deleteCurrency(currencyId);

        return ResponseEntity.ok(HttpResponseBody.<Void>builder()
            .message("Currency removed successfully")
            .build());
    }
}
