package by.manfin.manfinback.controllers;

import by.manfin.manfinback.model.HttpResponseBody;
import by.manfin.manfinback.model.Payment;
import by.manfin.manfinback.model.user.User;
import by.manfin.manfinback.services.PaymentService;
import by.manfin.manfinback.validation.Marker;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collection;


@RequestMapping("/documents/{documentId}/payments")
@RestController
@Validated
@SecurityRequirement(name = "bearer")
public class PaymentController {

    private final PaymentService paymentService;

    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @GetMapping
    public ResponseEntity<HttpResponseBody<Collection<Payment>>> getAllPayments(
            @AuthenticationPrincipal User user,
            @NotNull @PathVariable Long documentId
    ) {
        var payments = paymentService.getAllPayments(user, documentId);

        return ResponseEntity.ok(HttpResponseBody.<Collection<Payment>>builder()
                .data(payments)
                .build());
    }

    @GetMapping("/{paymentId}")
    public ResponseEntity<HttpResponseBody<Payment>> getPayment(
            @AuthenticationPrincipal User user,
            @NotNull @PathVariable Long documentId,
            @NotNull @PathVariable Long paymentId
    ) {
        var payment = paymentService.getPayment(user, documentId, paymentId);

        return ResponseEntity.ok(HttpResponseBody.<Payment>builder()
                .data(payment)
                .build());
    }

    @PostMapping
    @Validated(Marker.OnCreate.class)
    public ResponseEntity<HttpResponseBody<Payment>> createPayment(
            @NotNull @PathVariable Long documentId,
            @RequestBody @Valid Payment payment,
            @AuthenticationPrincipal User user
    ) {
        var result = paymentService.createPayment(user, documentId, payment);

        return ResponseEntity.ok(HttpResponseBody.<Payment>builder()
                .message("Payment saved successfully")
                .data(result)
                .build());
    }

    @PutMapping("/{paymentId}")
    public ResponseEntity<HttpResponseBody<Payment>> updatePayment(
            @AuthenticationPrincipal User user,
            @NotNull @PathVariable Long documentId,
            @NotNull @PathVariable Long paymentId,
            @RequestBody @Valid Payment paymentData
    ) {
        var result = paymentService.updatePayment(user, documentId, paymentId, paymentData);

        return ResponseEntity.ok(HttpResponseBody.<Payment>builder()
                .message("Payment updated successfully")
                .data(result)
                .build());
    }

    @DeleteMapping("/{paymentId}")
    public ResponseEntity<HttpResponseBody<Void>> deletePayment(
            @NotNull @PathVariable Long documentId,
            @NotNull @PathVariable Long paymentId,
            @AuthenticationPrincipal User user
    ) {
        paymentService.deletePayment(user, documentId, paymentId);

        return ResponseEntity.ok(HttpResponseBody.<Void>builder()
                .message("Payment removed successfully")
                .build());
    }
}
