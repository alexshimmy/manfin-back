package by.manfin.manfinback.controllers;

import by.manfin.manfinback.model.HttpResponseBody;
import by.manfin.manfinback.model.RefreshToken;
import by.manfin.manfinback.model.authentication.AuthTokenData;
import by.manfin.manfinback.model.authentication.UsernamePasswordRequest;
import by.manfin.manfinback.model.user.User;
import by.manfin.manfinback.services.UserService;
import by.manfin.manfinback.validation.Marker;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Validated
@RequestMapping("/user")
@RestController
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    @Validated({Marker.OnCreate.class})
    public ResponseEntity<HttpResponseBody<Void>> register(@RequestBody @Valid User user) {
        userService.register(user);

        return ResponseEntity.ok(HttpResponseBody.<Void>builder()
                .message("User has been registered")
                .build());
    }

    @PostMapping("/auth")
    public ResponseEntity<HttpResponseBody<AuthTokenData>> auth(@RequestBody @Valid UsernamePasswordRequest usernamePasswordRequest) {
        var authTokenData = userService.authentication(usernamePasswordRequest);
        if (authTokenData == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(HttpResponseBody.<AuthTokenData>builder()
                    .message("Wrong user credentials")
                    .build());
        }

        return ResponseEntity.ok(HttpResponseBody.<AuthTokenData>builder()
                .message("User has been authorized successfully")
                .data(authTokenData)
                .build());
    }

    @PostMapping("/refreshtoken")
    public ResponseEntity<HttpResponseBody<AuthTokenData>> refresh(@RequestBody @Valid RefreshToken token) {
        var authTokenData = userService.refreshToken(token.getToken());

        return ResponseEntity.ok(HttpResponseBody.<AuthTokenData>builder()
                .data(authTokenData)
                .build());
    }

    @SecurityRequirement(name = "bearer")
    @PutMapping
    public ResponseEntity<HttpResponseBody<User>> update(
            @RequestBody @Valid User userData,
            @AuthenticationPrincipal User user
    ) {
        var updatedUser = userService.update(user.getUsername(), userData);

        return ResponseEntity.ok(HttpResponseBody.<User>builder()
                .message("User data was updated")
                .data(updatedUser)
                .build());
    }
}
