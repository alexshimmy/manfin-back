package by.manfin.manfinback.controllers;

import by.manfin.manfinback.model.HttpResponseBody;
import by.manfin.manfinback.model.Document;
import by.manfin.manfinback.model.user.User;
import by.manfin.manfinback.services.DocumentService;
import by.manfin.manfinback.validation.Marker;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collection;


@RequestMapping("/documents")
@RestController
@Validated
@SecurityRequirement(name = "bearer")
public class DocumentController {

    private final DocumentService documentService;

    public DocumentController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @GetMapping
    public ResponseEntity<HttpResponseBody<Collection<Document>>> getAllDocuments(@AuthenticationPrincipal User user) {
        var documents = documentService.getAllDocuments(user);

        return ResponseEntity.ok(HttpResponseBody.<Collection<Document>>builder()
                .data(documents)
                .build());
    }

    @GetMapping("/{documentId}")
    public ResponseEntity<HttpResponseBody<Document>> getDocument(
            @AuthenticationPrincipal User user,
            @NotNull @PathVariable Long documentId
    ) {
        var dbDocument = documentService.getDocument(user, documentId);

        return ResponseEntity.ok(HttpResponseBody.<Document>builder()
                .data(dbDocument)
                .build());
    }

    @PostMapping
    @Validated(Marker.OnCreate.class)
    public ResponseEntity<HttpResponseBody<Document>> create(
            @RequestBody @Valid Document document,
            @AuthenticationPrincipal User user
    ) {
        var result = documentService.createDocument(user, document);

        return ResponseEntity.ok(HttpResponseBody.<Document>builder()
                .message("Document saved successfully")
                .data(result)
                .build());
    }

    @PutMapping("/{documentId}")
    public ResponseEntity<HttpResponseBody<Document>> update(
            @AuthenticationPrincipal User user,
            @NotNull @PathVariable Long documentId,
            @RequestBody @Valid Document documentData
    ) {
        var result = documentService.updateDocument(user, documentId, documentData);

        return ResponseEntity.ok(HttpResponseBody.<Document>builder()
                .message("Document updated successfully")
                .data(result)
                .build());
    }

    @DeleteMapping("/{documentId}")
    public ResponseEntity<HttpResponseBody<Void>> delete(
            @NotNull @PathVariable Long documentId,
            @AuthenticationPrincipal User user
    ) {
        documentService.deleteDocument(user, documentId);

        return ResponseEntity.ok(HttpResponseBody.<Void>builder()
                .message("Document removed successfully")
                .build());
    }
}
