package by.manfin.manfinback.controllers;

import by.manfin.manfinback.model.HttpResponseBody;
import by.manfin.manfinback.validation.ValidationErrorResponse;
import by.manfin.manfinback.validation.Violation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class ErrorHandlingControllerAdvice {

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<HttpResponseBody<ValidationErrorResponse>> onConstraintValidationException(ConstraintViolationException e) {
        final List<Violation> violations = e.getConstraintViolations().stream()
                .map(
                    violation -> new Violation(
                        violation.getPropertyPath().toString(),
                        violation.getMessage()
                    )
                ).collect(Collectors.toList());

        log.warn(String.format("ConstraintViolationException: %s", violations));

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(HttpResponseBody.<ValidationErrorResponse>builder()
                .message("Please make sure request data is correct")
                .data(new ValidationErrorResponse(violations))
                .build());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<HttpResponseBody<ValidationErrorResponse>> onMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        final List<Violation> violations = e.getBindingResult().getFieldErrors().stream()
                .map(violation -> new Violation(violation.getField(), violation.getDefaultMessage()))
                .collect(Collectors.toList());

        log.warn(String.format("MethodArgumentNotValidException: %s", violations));

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(HttpResponseBody.<ValidationErrorResponse>builder()
                .message("Method argument is not valid")
                .data(new ValidationErrorResponse(violations))
                .build());
    }
}
