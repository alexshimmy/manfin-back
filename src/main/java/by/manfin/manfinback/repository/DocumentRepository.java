package by.manfin.manfinback.repository;

import by.manfin.manfinback.model.Document;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DocumentRepository extends JpaRepository<Document, Long> {}
