package by.manfin.manfinback.repository;

import by.manfin.manfinback.model.Currency;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CurrencyRepository extends JpaRepository<Currency, Long> {

    Optional<Currency> findByShortName(String name);
}
