package by.manfin.manfinback.repository;

import by.manfin.manfinback.model.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<Payment, Long> {}
