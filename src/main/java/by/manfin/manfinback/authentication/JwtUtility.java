package by.manfin.manfinback.authentication;

import by.manfin.manfinback.model.authentication.AuthTokenData;
import by.manfin.manfinback.model.user.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.DefaultClaims;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.*;

@Slf4j
@Component
public class JwtUtility {

    @Value("${jwt.secret}")
    private String secret;
    @Value("${jwt.expiration}")
    private Long expirationTime;
    @Value("${jwt.refresh-token-expiration}")
    private Long refreshTokenExpirationTime;

    public Optional<String> extractUsername(String authToken) {
        try {
            return Optional.of(getClaimsFromToken(authToken).orElseThrow().getSubject());
        } catch (NoSuchElementException ex) {
            log.error(String.format("Unable to extract username from token: <%s>: token claims are empty", authToken));

            return Optional.empty();
        }
    }

    public boolean isTokenExpired(String authToken) {
        try {
            return getClaimsFromToken(authToken)
                    .orElseThrow()
                    .getExpiration()
                    .before(new Date());
        } catch (NoSuchElementException ex) {
            log.error("Unable to check token expiration: token claims are empty");

            return true;
        }
    }

    public boolean validateToken(String token, User user) {
        if (user == null || token == null) {
            return false;
        }

        return !isTokenExpired(token) && user.isEnabled();
    }

    public boolean isRefreshToken(String token) {
        var tokenClaims = getClaimsFromToken(token).orElse(new DefaultClaims());

        return tokenClaims.containsKey("type") && tokenClaims.get("type").equals(TokenType.REFRESH.name());
    }

    public Optional<AuthTokenData> generateTokens(UserDetails user) {
        if (user == null) {
            log.error("Unable to generate token. User parameter value == null");

            return Optional.empty();
        }
        try {
            Date creationDate = new Date();
            Date tokenExpiration = new Date(creationDate.getTime() + expirationTime * 1000);
            Date refreshTokenExpiration = new Date(creationDate.getTime() + refreshTokenExpirationTime * 1000);

            HashMap<String, Object> tokenClaims = new HashMap<>();
            tokenClaims.put("type", TokenType.ACCESS);
            var authToken = Jwts.builder()
                    .setClaims(tokenClaims)
                    .setSubject(user.getUsername())
                    .setIssuedAt(creationDate)
                    .setExpiration(tokenExpiration)
                    .signWith(Keys.hmacShaKeyFor(secret.getBytes()))
                    .compact();

            HashMap<String, Object> refreshTokenClaims = new HashMap<>();
            refreshTokenClaims.put("type", TokenType.REFRESH);
            var refreshToken = Jwts.builder()
                    .setClaims(refreshTokenClaims)
                    .setSubject(user.getUsername())
                    .setIssuedAt(creationDate)
                    .setExpiration(refreshTokenExpiration)
                    .signWith(Keys.hmacShaKeyFor(secret.getBytes()))
                    .compact();

            return Optional.of(new AuthTokenData(authToken, refreshToken, tokenExpiration));
        } catch (Exception ex) {
            log.error(String.format("Unable to generate tokens. Username: %s", user.getUsername()));

            return Optional.empty();
        }
    }

    private Optional<Claims> getClaimsFromToken(String authToken) {
        try {
            String key = Base64.getEncoder().encodeToString(secret.getBytes());

            return Optional.of(
                    Jwts.parserBuilder()
                    .setSigningKey(key)
                    .build()
                    .parseClaimsJws(authToken)
                    .getBody());
        } catch (Exception ex) {
            log.error(String.format("Unable to get claims from token <%s>", authToken));

            return Optional.empty();
        }
    }
}
