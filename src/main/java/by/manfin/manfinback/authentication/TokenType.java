package by.manfin.manfinback.authentication;

public enum TokenType {
    REFRESH, ACCESS
}
