CREATE SCHEMA IF NOT EXISTS manfin;

CREATE TABLE IF NOT EXISTS manfin.currency
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY,
    name character varying (100) NOT NULL,
    short_name character varying (100) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (short_name)
);

CREATE TABLE IF NOT EXISTS manfin.users (
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY,
    firstname character varying (100),
    lastname character varying (100),
    password character varying (100) NOT NULL,
    username character varying (100) NOT NULL,
    role character varying (15) NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (username)
);

CREATE TABLE IF NOT EXISTS manfin.documents (
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY,
    document_number character varying (100) NOT NULL,
    user_id bigint NOT NULL,
    name character varying (100) NOT NULL,
    description character varying (500),
    sum numeric (50,2) NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    paid_out numeric (50,2) DEFAULT 0.00 NOT NULL,
    currency_id bigint NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (currency_id) REFERENCES manfin.currency (id),
    FOREIGN KEY (user_id) REFERENCES manfin.users (id)
);

CREATE TABLE IF NOT EXISTS manfin.payments
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY,
    document_id bigint NOT NULL,
    date date NOT NULL,
    sum numeric(50,2) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (document_id) REFERENCES manfin.documents (id)
);

CREATE TABLE IF NOT EXISTS manfin.refresh_tokens
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY,
    user_id bigint NOT NULL,
    token character varying (200) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES manfin.users (id),
    UNIQUE (user_id)
);
