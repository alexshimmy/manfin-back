INSERT INTO manfin.currency (name, short_name) VALUES
('Доллар США', 'USD'),
('Белорусский рубль', 'BYN'),
('Российский рубль', 'RUR'),
('Казахстанский тенге', 'KZT')
ON CONFLICT DO NOTHING;
